use serde::Deserialize;
use std::error;
use std::process::{Command, Stdio};
type Result<T> = std::result::Result<T, Box<dyn error::Error>>;
struct Package {
    name: String,
    bin: Option<String>,
}

enum PackageType {
    NPM,
    NPX,
    Package,
    Binary,
}

enum PackageError {
    NotInstalled,
}

#[derive(Deserialize, Debug)]
struct PackageJson {
    name: String,
    version: String,
    main: String,
    types: String,
}
trait Runnable {
    fn execute(&self, arg: &str) -> Result<()>;
    fn execute_interactive(&self, arg: &str) -> Result<()>;
    fn execute_and_return_output(&self, arg: &str) -> Result<String>;
    fn version(&self) -> Result<String>;
    fn installed(&self) -> Result<bool>;
}

impl Package {
    pub fn new(name: &str, bin: Option<&str>) -> Package {
        Package {
            name: name.to_string(),
            bin: match bin {
                Some(s) => Some(s.to_string()),
                None => None,
            },
        }
    }
    fn install(&self) -> Result<()> {
        let npm = Package {
            name: "npm".to_string(),
            bin: Some("npm".to_string()),
        };
        match npm.execute(&format!("npm install {}", &self.name)) {
            Ok(_) => Ok(()),
            Err(e) => Err(e),
        }
    }
    pub fn installed(&self) -> bool {
        match self.version() {
            Ok(_) => true,
            Err(_) => false,
        }
    }
    pub fn execute(&self, arg: &str) -> Result<()> {
        let mut output = self
            .program()
            .arg("-c")
            .arg(arg)
            .stdout(Stdio::inherit())
            .stderr(Stdio::inherit())
            .spawn()?;

        match output.wait() {
            Ok(_e) => Ok(()),
            Err(e) => Err(e.into()),
        }
    }
    pub fn execute_output(&self, arg: &str) -> Result<String> {
        let mut output = self
            .program()
            .arg("-c")
            .arg(arg)
            .stderr(Stdio::inherit())
            .output()?;

        Ok(std::str::from_utf8(&output.stdout)?.trim().to_string())
    }
    pub fn version(&self) -> Result<String> {
        let version = match self.name.as_str() {
            "npm" | "npx" => {
                let arg = [&self.bin.as_ref().unwrap().to_string(), "-v"].join(" ");
                match self.execute_output(&arg) {
                    Ok(s) => {
                        if s.len() > 0 {
                            Ok(s)
                        } else {
                            Err("No output from version check -v".into())
                        }
                    }
                    Err(e) => Err(e),
                }
            }
            _ => match self.package_json() {
                Ok(x) => Ok(x.version),
                Err(e) => Err(e),
            },
        }?;
        Ok(version)
    }
    fn package_json(&self) -> Result<PackageJson> {
        let file = std::fs::File::open(format!("./node_modules/{}/package.json", self.name))?;
        let reader = std::io::BufReader::new(file);
        let json = serde_json::from_reader(reader)?;
        Ok(json)
    }
    fn program(&self) -> Command {
        if cfg!(target_os = "windows") {
            Command::new("cmd")
        } else {
            Command::new("sh")
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn get_version() {
        let npm = Package::new("npm", Some("npm"));
        let version = npm.version().unwrap();
        assert!(version.len() > 1);
    }
    #[test]
    fn get_uninstalled_package() {
        let x = Package::new("test", Some("test"));
        assert!(x.version().is_err());
    }
    #[test]
    fn install_package() {
        let buidler = Package::new("@nomiclabs/buidler", Some("buidler"));
        match buidler.installed() {
            true => println!("Allready insatlled buidler"),
            false => buidler.install().expect("Buidler did not install"),
        };
        assert!(buidler.installed());
    }
}
