# Project Title

A Rust package to learn Rust and use/install npm packages.

## Getting Started

`let npm = Package::new("npm", Some("npm"));`

`let version = npm.version().unwrap();`

`let buidler = Package::new("@nomiclabs/buidler", Some("buidler"));`
`assert!(buidler.installed());`

## Built With

## Contributing

## Versioning

## Authors

- **Robertosnap** - _Initial work_ - [Robertosnap](https://gitlab.com/robertosnap)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

- Hat tip to anyone whose code was used
